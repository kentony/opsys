#include <stdio.h>
#include <string.h>

void rec_brute(char * alph, char * ans, int pos){
    if (!ans[pos])
        printf("%s\n", ans);
    else {
        char * a;
        for (a = alph; *a; ++a) {
            ans[pos] = *a;
            rec_brute(alph, ans, pos + 1); 
        }
    }
}

void it_brute(){
    
}


int main(int argc, char * argv[]){
    char * alph = "abc";
    int length = 4;
    char ans[length + 1];
    int i;
    
    for (i = 0; i < length; ++i)
      ans[i] = alph[0];
    ans[length] = 0;
    rec_brute(alph, ans, 0);
    return 0;
}
